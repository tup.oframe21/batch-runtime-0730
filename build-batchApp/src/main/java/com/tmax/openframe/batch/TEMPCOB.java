package com.tmax.openframe.batch;

import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import com.tmax.openframe.batch.runtime.data.DdDO;
import java.util.Map;

/** 
 * <p>This source code was generated by COBOL to JAVA migrator</p>
 */
@Getter
@Setter
public class TEMPCOB implements Tasklet {
    private Map<String, DdDO> dd;
    private String parm;

    @Override
    public RepeatStatus execute(StepContribution contribution,
            ChunkContext chunkContext) throws Exception {
        com.tmax.openframe.TEMPCOB program = new com.tmax.openframe.TEMPCOB();
        program.run();
        return RepeatStatus.FINISHED;
    }
}
