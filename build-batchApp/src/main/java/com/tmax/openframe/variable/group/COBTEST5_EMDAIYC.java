package com.tmax.openframe.variable.group;

import ch.obermuhlner.math.big.BigDecimalMath;
import java.math.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.function.BiFunction;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

/** 
 * <p>This source code was generated by COBOL to JAVA migrator</p>
 */
public @AllArgsConstructor @NoArgsConstructor @Builder(builderClassName = "builder") class COBTEST5_EMDAIYC {
    public @Getter @Setter @Builder.Default COBTEST5_DL_KEY DL_KEY = new COBTEST5_DL_KEY();
    public @Getter @Builder.Default String DL_LCYM = StringUtils.repeat(' ', 6);
    public @Getter @Setter int DL_LPMT;
    private final static BiFunction<Long, String, String> intFormatWithoutSign = (
            value, format) -> {
        DecimalFormat nf = new DecimalFormat();
        nf.setNegativePrefix("");
        nf.applyPattern(format);
        return nf.format(value);
    };

    public void setDL_LCYM(String source) {
        DL_LCYM = StringUtils.truncate(StringUtils.rightPad(source, 6), 6);
    }

    public String getStringDL_LPMT() {
        return intFormatWithoutSign.apply((long) DL_LPMT, "00000");
    }

    @Override
    public String toString() {
        return getDL_KEY().toString() + getDL_LCYM() + getStringDL_LPMT();
    }
}
