#!/bin/sh

TARGET_DIR=/home/oflab/migrator2

cp -r $TARGET_DIR/jcl/instream target/.
cp -r $TARGET_DIR/jcl/xml target/.
cp -r $TARGET_DIR/cobol/* build-batchApp/src/main/java/.
cp -r $TARGET_DIR/batch-context.xml target/.
